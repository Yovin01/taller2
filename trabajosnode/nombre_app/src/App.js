import React from 'react';
import './App.css';
import {Navigate, Route, Routes, useLocation} from 'react-router-dom';
import Sesion from './fragment/Sesion';
import Inicio from './fragment/Inicio';
import Vendidos from './fragment/PresentarAutoV';
import PresentarAuto, { Prueba } from './fragment/PresentarAuto';
import EditarAuto from './fragment/EditarAuto';
import { borrarSesion, estaSesion } from './utilidades/Sessionutil';
import VentasRepuestos from './fragment/VentasRepuestos';
import PresentarAutoPersona from './fragment/PresentarAutoPersona';

function App() {
  const Middeware = ({children}) =>{
    const autenticado = estaSesion();
    const location = useLocation();
    if(autenticado){
      return children;
    }else{
      return <Navigate to= '/sesion' state={location}/>;
    }
  }


  const MiddewareSesion = ({children}) =>{
    const autenticado = estaSesion();
    const location = useLocation();
   // borrarSesion();
    if(autenticado){
      return <Navigate to= '/inicio'/>;
      
    }else{
      return children;
    }
  }
  return (
    <div className="App">
      <Routes>
        <Route path='/sesion' element={<MiddewareSesion><Sesion/></MiddewareSesion>}/>
        <Route path='/inicio' element={<Middeware><Inicio/></Middeware>}/>
        <Route path='/autos' element={<Prueba/>}/>
        <Route path='/autosV' element={<Vendidos/>}/>
        <Route path='/autosDue' element={<PresentarAutoPersona/>}/>
        <Route path='/autos/edicion' element={<EditarAuto/>}/>
        <Route path='/ordesCompra' element={<VentasRepuestos/>}/>
      </Routes>
      </div>
  );
}

export default App;
