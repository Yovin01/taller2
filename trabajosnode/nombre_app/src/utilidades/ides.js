export const saveID = (id, uno, marca) => {
    localStorage.setItem('id', id);
    localStorage.setItem('uno', uno);
    localStorage.setItem('marca', marca);
};

export const getID = () => {
    const id = localStorage.getItem('id');
    const uno = localStorage.getItem('uno');
    return { id, uno };
};


export const eliminarID = () => {
    localStorage.removeItem('id');
    localStorage.removeItem('uno');
};

export const saveIdenti = (id) => {
    localStorage.setItem('identificacion', id);
};

export const getIdenti = () => {
    const id = localStorage.getItem('identificacion');
    return id;
};
export const eliminarIdenti = () => {
    localStorage.removeItem('identificacion');
};

export const elimianrTodo = () => {
    localStorage.clear();
};

export const eliminarRepuesto = () => {
    localStorage.removeItem('repuesto');
};

export const saveRepuesto = (repuestos) => {
        localStorage.setItem('repuesto', JSON.stringify(repuestos));
  };
  
  export const getRepuesto = () => {
    const repuestos = localStorage.getItem('repuesto');
  //  return JSON.parse(repuestos) ;
    return repuestos ;
  };




