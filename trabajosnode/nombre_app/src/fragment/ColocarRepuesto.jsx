import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import mensajes from "../utilidades/Mensajes";
import { useForm } from 'react-hook-form';
import '../css/style.css';
import { GuardarAuto, ListarRepuestos } from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import { eliminarRepuesto, getRepuesto, saveRepuesto } from '../utilidades/ides';
function ColocarRepuesto() {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const navegation = useNavigate();
    const [titulo, setTitulo] = useState([]);
    const [lltitulo, setLltitulo] = useState(false);
    const [nro, setNro] = useState(0);
    const [datas, setData] = useState([]);
    const [dataM, setDataM] = useState([]);
    const [llLIbros, llsetLibros] = useState(false);

    if (!llLIbros) {

        ListarRepuestos(getToken()).then((info) => {
          //  console.log(info);
            if (info.code !== 200 && (info.msg === "No existe token" || info.msg === "Token no valido")) {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion")
            } else {
            //    console.log(info);
                llsetLibros(true);
                setDataM(info.info);
                if (JSON.parse(getRepuesto()) !== null) {
                    setData(JSON.parse(getRepuesto()));
                }
               // console.log(dataM);
            }
        })
    }

    const onSubmit = (data) => {
        const marcaSeleccionada = data.marca;
        const marca = dataM.find((marca) => marca.nombre === marcaSeleccionada);
        console.log(marca);
        if (marca !== undefined) {
            var ac  = datas.some((element) => element.external_id === marca.external_id);
            if (!ac) {
                const newData = datas.concat(marca); // Concatenar marca a los datos existentes
                setData(newData); // Actualizar el estado con los nuevos datos
                saveRepuesto(newData); // Guardar los nuevos datos en localStorage
            } else {
                mensajes("Repuesto ya agregado", 'error', 'Error');
            }
        } else {
            mensajes("Seleccione un repuesto", 'error', 'Error');
        }
     //   console.log(JSON.parse(getRepuesto()));

        // Cerrar la ventana emergente
    };
    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                    {/** INGRESAR marca */}
                                    <div className="form-group">
                                        <select className="form-control " {...register('marca', { required: true })}>
                                            <option >Seleccione un repuesto</option>
                                            {dataM.map((marca) => (
                                                <option value={marca.nombre}>{marca.nombre}</option>
                                            ))}
                                        </select>
                                        {errors.marca && errors.marca.type === 'required' && <div className='alert alert-danger'></div>}
                                    </div>
                                    <hr />
                                    {/** BOTÓN CANCELAR */}
                                    <div style={{ display: 'flex', gap: '10px' }}>
                                        {/** BOTÓN REGISTRAR */}
                                        <input className="btn btn-success btn-rounded" type='submit' value='Agregar' ></input>
                                    </div>
                                </form>
                                <hr />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default ColocarRepuesto;