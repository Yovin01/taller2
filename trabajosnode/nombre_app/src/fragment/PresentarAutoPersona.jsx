import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal } from 'react-bootstrap';
import DataTable from "react-data-table-component";
import React, { useState } from 'react';
import { AutosV, ObtenerAutoDue } from "../hooks/Conexion";
import { borrarSesion, getToken } from "../utilidades/Sessionutil";
import mensajes from "../utilidades/Mensajes";
import { useNavigate } from "react-router";
import { getID, getIdenti, saveID } from "../utilidades/ides";
import AgregarAuto from "./AgregarAuto";
const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;

const columns = [
    {
        name: 'Modelo',
        selector: row => row.auto.modelo,
        wrap: true,
    },

    {
        name: 'Color',
        selector: row => row.auto.color,
        wrap: true,
    },
    {
        name: 'Placa',
        selector: row => row.auto.placa,
        wrap: true,
    },

    {
        name: 'Marca',
        selector: row => row.auto.marca.nombre,
        wrap: true,
    }, {
        name: 'Precio',
        selector: row => '$ ' + row.auto.precio,
        wrap: true,
    },

    {




        /* 
        
         
     },
        
        name: 'Acciones',
         selector: row => (<>
         
             <div style={{ display: 'flex', gap: '10px' }}>
                 <a href="/autos/edicion" class="btn btn-outline-info btn-rounded">
                     <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square" viewBox="0 0 16 16">
                         <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                         <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                     </svg>
                 </a>
 
                 <a href="/autos" className="btn btn-outline-danger btn-rounded">
                     <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-circle" viewBox="0 0 16 16">
                         <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                         <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                     </svg>
                 </a>
             </div>
             
         </>),*/
    },

];


export const PresentarAutoPersona = () => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [data, setData] = useState([]);
    const navegation = useNavigate();
    const [llLIbros, llsetLibros] = useState(false);
    const [searchText, setSearchText] = useState('');
    const [filteredData, setFilteredData] = useState([]);
    const [selectedBook, setSelectedBook] = useState(null);
    const [numeroFila, setNumeroFila] = useState(0);
    if (!llLIbros) {

        ObtenerAutoDue(getToken(), getIdenti()).then((info) => {
          //  console.log(info);
            if (info.code !== 200 && (info.msg === "No existe token" || info.msg === "Token no valido")) {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion")
            } else {
                console.log(info);
                llsetLibros(true);
                setData(info.info);
               
            }
        })
    }
    const handleBookClick = (row) => {
        const { auto } = row;
        setSelectedBook(auto);
        setNumeroFila(auto.placa);
       console.log(auto);
        saveID(auto.factura.numeroF, 0, auto.marca.nombre);
     //   console.log(getID());
        handleShow(true);
   navegation('/ordesCompra');

    };
    const handleSearch = (e) => {
        const searchValue = e.target.value;
        setSearchText(searchValue);
        const filteredBooks = data.filter(book =>
            book.title.toLowerCase().includes(searchValue.toLowerCase()));
        setFilteredData(filteredBooks);
    };


    return (

        <div className="container">
            <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                <div className="row ">

                    <div className="col-sm-3 mt-5 mb-4 text-gred">
                        <div className="search">
                            <form className="form-inline">
                                <input
                                    className="form-control mr-sm-2"
                                    type="search"
                                    placeholder="Buscar Auto"
                                    aria-label="Search"
                                    value={searchText}
                                    onChange={handleSearch} />
                            </form>
                        </div>
                    </div>
                    <div className="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred" style={{ color: "blue" }}>
                        <h2><b>Detalles de Autos</b></h2></div>
                </div>
                <div className="row">

                    <DataTable
                        columns={columns}
                        data={data}
                        selectableRows
                        onRowClicked={handleBookClick}
                    /> <div className="model_box">
   </div> </div></div>  </div>
    );}


export default PresentarAutoPersona;