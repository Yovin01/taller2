import React, { useState } from 'react';
import { ExternalPerson, Marcas, ModificarAuto, ModificarLibro, ObtenerAuto, ObtenerBook } from '../hooks/Conexion';
import { useNavigate } from 'react-router';
import mensajes from "../utilidades/Mensajes";
import { useForm } from 'react-hook-form';
import '../css/style.css';
import { eliminarID, getID } from '../utilidades/ides';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
function EditarAuto() {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const navegation = useNavigate();
    const [titulo, setTitulo] = useState([]);
    const [lltitulo, setLltitulo] = useState(false);
    const [data, setData] = useState(0);
    const [exteper,setPE] = useState(0);
    const [datas, setDatas] = useState([]);
    const [llLIbros, llsetLibros] = useState(false);
    const [marcasLL, setMarcasLL] = useState(false);
    const [dataM, setDataM] = useState([]);
    const { id, uno, marca12 } = getID();
    const [person, setPerson] = useState(false);
    //acciones
    if (!marcasLL) {

        Marcas(getToken()).then((info) => {
          //  console.log(info);
            if (info.code !== 200 && (info.msg === "No existe token" || info.msg === "Token no valido")) {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion")
            } else {
              //  console.log(info);
                setMarcasLL(true);
                setDataM(info.info);
            }
        })
    }

    if (!person) {
        ExternalPerson(getToken(), data.identificacion).then((info) => {
            //  console.log(info);
              if (info.code !== 200 && (info.msg === "No existe token" || info.msg === "Token no valido")) {
                  borrarSesion();
                  mensajes(info.msg);
                  navegation("/sesion")
              } else {
                //  console.log(info);
                  setPerson(true);
                  setPE(info.info);
              }
          })
    }

    if (!llLIbros) {
       
      //  console.log(uno);
        ObtenerAuto(getToken(), id, uno).then((info) => {
      console.log();
            if (info.code !== 200 && (info.msg === "No existe token" || info.msg === "Token no valido")) {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion")
            } else {
                setData(info.info);
                llsetLibros(true);
               // console.log(data.marca);
            }
        })
    }


    
    // onsubmit
    const onSubmit = (datas) => {
        const marcaSeleccionada = data.marca;
        const marca = dataM.find((marca) => marca.nombre === marcaSeleccionada);
        const externalId = marca ? marca.external_id : "";
        var datos;
          if(uno === '1'){
          datos  = {
                "placa": datas.placa,
                "color": datas.color,
                "precio": datas.precio,
                "exterlM": externalId,
                "externalPersona": exteper.external_id,
                "modelo": datas.modelo
            };
        }
        ModificarAuto(datos, getID()).then((info) => {
            if (info.error === true) {
                mensajes(info.message, 'error', 'Error');
                //msgError(info.message);            
            } else {
                mensajes(info.msg);
                eliminarID();
                navegation('/presentarLibro');
            }
        }
        );

    }; const handlePageCountChange = (e) => {
        const value = e.target.value.replace(/\D/g, ''); // Elimina cualquier caracter que no sea un número
        e.target.value = value;
    };
    const handlePriceChange = (e) => {
        const value = e.target.value.replace(/[^0-9.]/g, ''); // Permite solo números y decimales
        e.target.value = value;
    };

    return (
   
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                    {/** INGRESAR placa */}
                                    <div className="form-group">
                                        <input type="text" {...register('placa', { required: true })} className="form-control form-control-user" placeholder="Ingrese la placa" defaultValue={data.placa}  />
                                        {errors.placa && errors.placa.type === 'required' && <div className='alert alert-danger'>Ingrese la placa</div>}
                                    </div>
                                    {/** INGRESAR modelo */}
                                    <div className="form-group">
                                        <input type="text" {...register('modelo', { required: true })} className="form-control form-control-user" placeholder="Ingrese el modelo" defaultValue={data.modelo} />
                                        {errors.modelo && errors.modelo.type === 'required' && <div className='alert alert-danger'>Ingrese un modelo</div>}
                                    </div>
                                    {/** INGRESAR color */}
                                    <div className="form-group">
                                        <input type="text" {...register('color', { required: true })} className="form-control form-control-user" placeholder="Ingrese el color" defaultValue={data.color}/>
                                        {errors.color && errors.color.type === 'required' && <div className='alert alert-danger'>Ingrese un color</div>}
                                    </div>
                                    {/** INGRESAR numeroF */}
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el numero factura" {...register('numeroF', { required: true })} onChange={handlePageCountChange} defaultValue={data.numeroF} />
                                       
                                    </div>
                                    {/** INGRESAR cantidad */}
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese la cantidad" {...register('cantidad', { required: true })} onChange={handlePageCountChange } defaultValue={data.cantidad}/>
                                       
                                    </div>
                                    {/** INGRESAR precio */}
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el precio" {...register('precio', { required: true })} onChange={handlePriceChange} defaultValue={data.precio} />
                                        {errors.pageCount && errors.pageCount.type === 'required' && <div className='alert alert-danger'>Ingrese el precio</div>}
                                    </div>
                                    {/** INGRESAR marca */}
                                    <div className="form-group">
                                        <select className="form-control " {...register('marca', { required: true })} >
                                            <option defaultValue="" >Seleccione una marca</option>
                                            {dataM.map((marca) => (
                                                <option value={marca.nombre}>{marca.nombre}</option>
                                            ))}
                                        </select>
                                        {errors.marca && errors.marca.type === 'required' && <div className='alert alert-danger'>Seleccione una marca</div>}
                                    </div>
                                    {/** INGRESAR identificacion */}
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese la identificacion del duenio" {...register('identificacion', { required: true })} onChange={handlePageCountChange} defaultValue={data.identificacion}/>
                                       
                                    </div>
                                    <hr />
                                    {/** BOTÓN CANCELAR */}
                                    <div style={{ display: 'flex', gap: '10px' }}>
                                        <a href="/inicio" className="btn btn-danger btn-rounded">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                                            </svg>
                                            <span style={{ marginLeft: '5px' }}>Cancelar</span>
                                        </a>

                                        {/** BOTÓN REGISTRAR */}
                                        <input className="btn btn-success btn-rounded" type='submit' value='Registrar'></input>
                                    </div>

                                </form>
                                <hr />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default EditarAuto;