import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import mensajes from "../utilidades/Mensajes";
import { useForm } from 'react-hook-form';
import '../css/style.css';
import { GuardarAuto, Marcas } from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
function AgregarAuto() {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const navegation = useNavigate();
    const [titulo, setTitulo] = useState([]);
    const [lltitulo, setLltitulo] = useState(false);
    const [nro, setNro] = useState(0);
    const [data, setData] = useState([]);
    const [dataM, setDataM] = useState([]);
    const [llLIbros, llsetLibros] = useState(false);
    if (!llLIbros) {

        Marcas(getToken()).then((info) => {
            console.log(info);
            if (info.code !== 200 && (info.msg === "No existe token" || info.msg === "Token no valido")) {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion")
            } else {
                console.log(info);
                llsetLibros(true);
                setDataM(info.info);
            }
        })
    }
    console.log(dataM);
    const onSubmit = (data) => {
        const marcaSeleccionada = data.marca;
        const marca = dataM.find((marca) => marca.nombre === marcaSeleccionada);
        const externalId = marca ? marca.external_id : "";
  //      console.log(externalId);

        var datos = {
            "numeroF": data.numeroF,
            "precioTotal": data.precio * data.cantidad,
            "detalle": "'{\"precio\":" + data.precio + ",\"cantidad\":" + data.cantidad + ",\"producto\":" + data.modelo + "\"}'", // data.detalle, //'{"precio":4000,"cantidad":1,"producto":"dest-de"}'
            "placa": data.placa,
            "color": data.color,
            "modelo": data.modelo,
            "precio": data.precio,
            "identificacion": data.identificacion,
            "externalM": externalId
        };
        GuardarAuto(datos).then((info) => {
            if (info.code !== 200) {
                mensajes(info.msg, 'error', 'Error');
                //msgError(info.message);            
            } else {
                mensajes(info.msg);
                navegation('/inicio');
            }
        });
    }; const handlePageCountChange = (e) => {
        const value = e.target.value.replace(/\D/g, ''); // Elimina cualquier caracter que no sea un número
        e.target.value = value;
    };
    const handlePriceChange = (e) => {
        const value = e.target.value.replace(/[^0-9.]/g, ''); // Permite solo números y decimales
        e.target.value = value;
    };

    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                    {/** INGRESAR placa */}
                                    <div className="form-group">
                                        <input type="text" {...register('placa', { required: true })} className="form-control form-control-user" placeholder="Ingrese la placa" />
                                        {errors.placa && errors.placa.type === 'required' && <div className='alert alert-danger'>Ingrese la placa</div>}
                                    </div>
                                    {/** INGRESAR modelo */}
                                    <div className="form-group">
                                        <input type="text" {...register('modelo', { required: true })} className="form-control form-control-user" placeholder="Ingrese el modelo" />
                                        {errors.modelo && errors.modelo.type === 'required' && <div className='alert alert-danger'>Ingrese un modelo</div>}
                                    </div>
                                    {/** INGRESAR color */}
                                    <div className="form-group">
                                        <input type="text" {...register('color', { required: true })} className="form-control form-control-user" placeholder="Ingrese el color" />
                                        {errors.color && errors.color.type === 'required' && <div className='alert alert-danger'>Ingrese un color</div>}
                                    </div>
                                    {/** INGRESAR numeroF */}
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el numero factura" {...register('numeroF', { required: true })} onChange={handlePageCountChange} />
                                        {errors.numeroF && errors.numeroF.type === 'required' && <div className='alert alert-danger'>Ingrese el numero de factura</div>}
                                    </div>
                                    {/** INGRESAR cantidad */}
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese la cantidad" {...register('cantidad', { required: true })} onChange={handlePageCountChange} />
                                        {errors.cantidad && errors.cantidad.type === 'required' && <div className='alert alert-danger'>Ingrese la cantidad</div>}
                                    </div>
                                    {/** INGRESAR precio */}
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el precio" {...register('precio', { required: true })} onChange={handlePriceChange} />
                                        {errors.pageCount && errors.pageCount.type === 'required' && <div className='alert alert-danger'>Ingrese el precia</div>}
                                    </div>
                                    {/** INGRESAR marca */}
                                    <div className="form-group">
                                        <select className="form-control " {...register('marca', { required: true })}>
                                            <option defaultValue="Seleccione una marca">Seleccione una marca</option>
                                            {dataM.map((marca) => (
                                                <option value={marca.nombre}>{marca.nombre}</option>
                                            ))}
                                        </select>
                                        {errors.marca && errors.marca.type === 'required' && <div className='alert alert-danger'>Seleccione una marca</div>}
                                    </div>
                                    {/** INGRESAR identificacion */}
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese la identificacion del duenio" {...register('identificacion', { required: true })} onChange={handlePageCountChange} />
                                        {errors.identificacion && errors.identificacion.type === 'required' && <div className='alert alert-danger'>Ingrese la identificacion del duenio</div>}
                                    </div>
                                    <hr />
                                    {/** BOTÓN CANCELAR */}
                                    <div style={{ display: 'flex', gap: '10px' }}>
                                        <a href="/inicio" className="btn btn-danger btn-rounded">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                                            </svg>
                                            <span style={{ marginLeft: '5px' }}>Cancelar</span>
                                        </a>

                                        {/** BOTÓN REGISTRAR */}
                                        <input className="btn btn-success btn-rounded" type='submit' value='Registrar'></input>
                                    </div>

                                </form>
                                <hr />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default AgregarAuto;