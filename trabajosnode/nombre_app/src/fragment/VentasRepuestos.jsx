import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Input } from 'react-bootstrap';
import RegistrarAuto from "./AgregarAuto";
import EditarAuto from "./EditarAuto";
import DataTable from "react-data-table-component";
import React, { useEffect, useState } from 'react';
import { AutosNV, FacCant, ListarRepuestos, ObtenerAuto, facturarRep } from "../hooks/Conexion";
import { borrarSesion, getToken } from "../utilidades/Sessionutil";
import mensajes from "../utilidades/Mensajes";
import { useNavigate } from "react-router";
import Footer from "./Footer";
import { getID, getRepuesto, saveRepuesto } from "../utilidades/ides";
import ColocarRepuesto from "./ColocarRepuesto";
import { DismiRepuestos } from "../hooks/Conexion";
import { AutosV } from "../hooks/Conexion";
import { useForm } from "react-hook-form";
import { eliminarRepuesto } from "../utilidades/ides";
export const VentasRepuestos = () => {
    const [show, setShow] = useState(false);
    const { id, uno } = getID();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const handleClose = () => {
        const repuesto = getRepuesto();
        console.log('aa-aa');
        console.log(getRepuesto());
        if (repuesto !== null) {
            setData(JSON.parse(getRepuesto()));
        }
        setShow(false);
    };
    const [iva, setIVA] = useState(0.12);
    const handleShow = () => setShow(true);
    const [data, setData] = useState([]);
    const [cantid, setcantid] = useState([]);
    const [datita, setDatita] =  useState(0);
    const [persona, setPersona] = useState([]);
    const [auto, setAuto] = useState([]);

    const [marca, setMarca] = useState([]);
    const navegation = useNavigate();
    const [llAutosNV, setLlAutosNV] = useState(false);//para listar AutosNV
    const actualizar = () => setLlAutosNV(false);//para listar AutosNV
    const [llVendidos, setLlVendidos] = useState(false);//para listar vendidos
    const vendidos = () => setLlVendidos(true);//para listar vendidos
    const [llDisponibles, setLlDisponibles] = useState(false);//para listar disponibles
    const disponibles = () => setLlDisponibles(true);//para listar disponibles
    const [selectedId, setSelectedId] = useState(null);//PARA SACAR EL ID DE LA TABLA
    const [show2, setShow2] = useState(false);//Model Box2
    const handleeClose = () => setShow2(false);//Model Box2
    const handleeShow = () => setShow2(true);//Model Box2
    const handleEditarAuto = async (id) => {
        setSelectedId(id);//Guarda el id en la variable selectedId
        handleeShow();//Llama a el model de editar
    };//PARA SACAR EL ID DE LA TABLA
    const handleEliminarAuto = async (id) => {
        const newData = data.filter((row) => row.external_id !== id);
        setData(newData);
        saveRepuesto(newData);
    };//para eliminar repuesto de la tabla
    const [showDetails, setShowDetails] = useState(false);

    const handleShow2 = () => {
        setShowDetails(true);
        // Aquí puedes realizar las acciones adicionales cuando se hace clic en el botón
    };



    const [selectedCant, setSelectedCant] = useState({});

    const handleIncrement = (rowId) => {
        setSelectedCant((prevCant) => ({
            ...prevCant,
            [rowId]: (prevCant[rowId] || 0) + 1,

        }));
        // setData(newData);
    };
    const [quantities, setQuantities] = useState({});
    const handleDecrement = (rowId) => {
        setSelectedCant((prevCant) => ({
            ...prevCant,
            [rowId]: prevCant[rowId] > 0 ? prevCant[rowId] - 1 : 0,
        }));
        // setData(newData);
    };
    const [selectedQuantity, setSelectedQuantity] = useState(1);
    const [subTotal, setsubTotal] = useState(0);
    const [precios, setPrecios] = useState([]);
    var total = 0;

    const handleQuantityChange = (event, externalId, maxCantidad, row) => {
        const value = event.target.value;
        if (value >= 1 && value <= maxCantidad) {
            setQuantities((prevQuantities) => ({
                ...prevQuantities,
                [externalId]: value
            }));
        } else if (value < 1) {
            setQuantities((prevQuantities) => ({
                ...prevQuantities,
                [externalId]: 1
            }));
        } else {
            setQuantities((prevQuantities) => ({
                ...prevQuantities,
                [externalId]: maxCantidad
            }));
        }

    };


    const calcularPrecioTotal = (row) => {
        const cantidad = quantities[row.external_id] || 0;
        const precioTotal = row.precio * cantidad;
        //  setsubTotal(subTotal+ row.precio * cantidad);
        return '$ ' + precioTotal;
    };
    const columns = [
        {
            name: 'Marca',
            selector: row => row.marca,
        },
        {
            name: 'Nombre',
            selector: row => row.nombre,
        },
        {
            name: 'Tipo categorio',
            selector: row => row.tipo_categoria,
        },
        {
            name: 'Precio',
            selector: row => row.precio,
        },
        {
            name: 'Cantidad',
            selector: row => (
                <div style={{ display: 'flex', gap: '5px', width: "400px" }}>
                    <input
                        id="form1"
                        min="1"
                        max={row.cantidad}
                        name="quantity"
                        value={quantities[row.external_id]}
                        type="number"
                        style={{ width: '100px', height: '20px' }}
                        className="form-control form-control-sm"
                        onChange={(event) =>
                            handleQuantityChange(event, row.external_id, row.cantidad, row)}
                    />
                </div>
            )
        },
        {
            name: 'Precio total',
            selector: row => calcularPrecioTotal(row),
        },
        {
            name: 'Acciones',

            selector: row => (
                <Button variant="btn btn-outline-danger btn-rounded" onClick={() => handleEliminarAuto(row.external_id)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                    </svg>
                </Button>
            ),
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
        },
    ];
    if (!llAutosNV) {
        ObtenerAuto(getToken(), id, 0).then((info) => {
            if (info.error === true || info.msg === 'Token no valido o expirado!') {
                borrarSesion();
                mensajes(info.msg);
                navegation("/sesion");
            } else {
                setAuto(info.info.auto[0]);
                setPersona(info.info.persona);
                setMarca(info.info.auto[0].marca);

            }
        })
        setLlAutosNV(true);
    }
    const precioTotalColumn = columns.find(column => column.name === 'Precio total');
//console.log(precioTotalColumn);
    if (precioTotalColumn) {
        //const preciosTotales = data.map(row => precioTotalColumn.selector(row));
        const preciosTotales = data.map(row => precioTotalColumn.selector(row)); // Obtener el array de precios totales
        total = preciosTotales.reduce((accumulator, currentValue) => accumulator + Number(currentValue.slice(1)), 0); // Sumar los números del array

        //console.log(total); // Mostrar la suma total
        //  setsubTotal();
    }
    const onSubmit = (datas) => {
        const cantidad = columns.find(column => column.name === 'Cantidad');
          const cantidades = data.map(row => cantidad.selector(row));
          var dat = '';
          for (let index = 0; index < data.length; index++) {
              dat = dat.concat('{nombre:' + data[index].nombre + ',precio:' + data[index].precio + ',cantidad:' + cantidades[index].props.children.props.value+ '}');
          }
          FacCant(getToken()).then((info) => {
              //  console.log(info);
              if (info.code !== 200 && (info.msg === "No existe token" || info.msg === "Token no valido")) {
                  borrarSesion();
                  mensajes(info.msg);
                //  navegation("/sesion")
              } else {
             console.log(info,info);
                  setDatita(info.info);
              }
          })
          var datos = {
              "numeroF": '000000' + (datita+1),
              "precioTotal": total * iva + total,
              "detalle": dat,
              "precio": total,
              "identificacion": persona.identificacion,
          };
  
          facturarRep(datos, getToken()).then((info) => {
              //  console.log(info);
              if (info.code !== 200) {
                      mensajes(info.msg, 'error', 'Error');
                      //msgError(info.message);            
                  } else {
                      mensajes(info.msg);
                      for (let index = 0; index < data.length; index++) {
                        const element = data[index].external_id;
                   //     console.log(cantidades[index].props.children.props.value);
                       // console.log(element);
                      //  console.log(precioTotalColumn[index]);
                        var datos = {
                            "external_id": element,
                            "cantidad": cantidades[index].props.children.props.value,
                        };
                        DismiRepuestos(datos, getToken()).then((info) => {
                            if (info.code !== 200) {
                                mensajes(info.msg, 'error', 'Error');
                                //msgError(info.message);            
                            } else {
                                mensajes(info.msg);
                            }
                        });
                    }
                    eliminarRepuesto();
                  //    navegation('/inicio');
                  }
          })
   
        //      console.log(externalId);
    };
    return (

        <div >
            <div style={{ width: '98%', height: '98%', padding: "25px", margin: '50px auto', padding: '20px', border: ' 1px ', "border-radius": "5px", "text-align": "center" }}>
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col">
                        <div className="card">
                            <div className="card-body p-4">
                                <div className="row">
                                    <div className="col-lg-7">
                                        <h5 className="mb-3">
                                            <a className="text-body">
                                                <i className="fas fa-long-arrow-alt-left me-2"></i>
                                                Detalles orden compra
                                            </a>
                                        </h5>
                                        <hr />
                                        <div className="d-flex justify-content-between align-items-center mb-4">
                                            <div className="row">
                                                <div className="mb-0" style={{ marginTop: '25px' }}>
                                                    <ul className="list-unstyled" style={{ textAlign: 'left' }}>
                                                        <li className="text-muted">Duenio: <span style={{ "color": "green" }} > {persona.nombres} {persona.apellidos}</span></li>
                                                        <li className="text-muted">Identificacion:<span style={{ "color": "green" }} > {persona.identificacion}</span></li>
                                                        <li className="text-muted">Auto</li>
                                                        <li className="text-muted">Marca: <span style={{ "color": "green" }} > {marca.nombre}</span></li>
                                                        <li className="text-muted">Placa: <span style={{ "color": "green" }} > {auto.placa}</span></li>
                                                        <li className="text-muted">Modelo: <span style={{ "color": "green" }} > {auto.modelo}</span></li>
                                                        <li className="text-muted">Color: <span style={{ "color": "green" }} > {auto.color}</span></li>
                                                        <li className="text-muted">Precio: <span style={{ "color": "green" }} > {auto.precio}</span></li>
                                                    </ul>
                                                </div>

                                            </div>
                                            <div>

                                                <div className="model_box">
                                                </div>
                                            </div>
                                            <div>
                                                <p className="mb-0">
                                                    <span className="text-muted">Sort by:</span>{" "}
                                                    <a href="#!" className="text-body">
                                                        price{" "}
                                                        <i className="fas fa-angle-down mt-1"></i>
                                                    </a>
                                                </p>
                                            </div>

                                        </div>
                                        {/* Tabla de los repuestos */}
                                        <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                                            <div className="row ">
                                                <div className="col-sm-4 offset-sm-1  mt-5 mb-4 text-gred">
                                                    <Button variant="primary" onClick={handleShow} style={{ "margin-left": "auto" }}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                                                            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                                                        </svg>
                                                        <span style={{ marginLeft: '5px' }}>Agregar Repuesto</span>
                                                    </Button>
                                                </div>
                                             
                                            </div>
                                            <div className="row ">
                                                <div style={{ color: "blue" }}>
                                                    <h2><b>Repuestos</b></h2>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <DataTable
                                                    columns={columns}
                                                    data={data}
                                                />

                                            </div>
                                            <Footer></Footer>

                                            {/* <!--- Model Box ---> */}
                                            <div className="model_box">
                                                <Modal
                                                    show={show}
                                                    onHide={handleClose}
                                                    backdrop="static"
                                                    keyboard={false}
                                                >
                                                    <Modal.Header closeButton>
                                                        <Modal.Title>Agregar auto</Modal.Title>
                                                    </Modal.Header>
                                                    <Modal.Body>
                                                        <ColocarRepuesto />
                                                    </Modal.Body>

                                                    <Modal.Footer>
                                                        <Button variant="secondary" onClick={handleClose} >
                                                            Cancelar
                                                        </Button>

                                                    </Modal.Footer>
                                                </Modal>



                                            </div>
                                        </div>

                                        {/* Resto del contenido del carrito */}
                                    </div>
                                    <div className="col-lg-5">
                                        <div className="card bg-primary text-white rounded-3">
                                            <div className="card-body" style={{ background: '#5d9fc5', borderRadius: '5px' }}>
                                                <div className="d-flex justify-content-between align-items-center mb-4">
                                                    <h5 className="mb-0">Carrito</h5>
                                                </div>

                                                {/* Resto del contenido del formulario */}

                                                <hr className="my-4" />

                                                <div className="d-flex justify-content-between">
                                                    <p className="mb-2" >Subtotal</p>
                                                    <p className="mb-2">$ {total}</p>
                                                </div>

                                                <div className="d-flex justify-content-between">
                                                    <p className="mb-2">Iva</p>
                                                    <p className="mb-2">$ {total * iva}</p>
                                                </div>

                                                <div className="d-flex justify-content-between mb-4">
                                                    <p className="mb-2">Total(Incl. taxes)</p>
                                                    <p className="mb-2">$ {total * iva + total}</p>
                                                </div>
                                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                                    <button
                                                        type="submit"
                                                        className="btn btn-info btn-block btn-lg"
                                                        style={{ background: '#393053', border: 'none' }}
                                                    >
                                                        <div className="d-flex justify-content-between">
                                                            <span>$ {total * iva + total}</span>
                                                            <span>
                                                                Facturar{" "}
                                                                <i className="fas fa-long-arrow-alt-right ms-2"></i>
                                                            </span>
                                                        </div>
                                                    </button>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
}


export default VentasRepuestos;
