const URL = "http://localhost/v1/index.php"
const URL2 = "http://localhost:3008/api"
export const InicioSesion = async (data) => {
    var cabeceras = {
        "Accept": 'aplication/json',
        "Content-Type": 'application/json'
    };
    const datos = await (await fetch(URL2 + "/sesion", {
        method: "POST",
        headers: cabeceras,
        body: JSON.stringify(data)
    })).json();
 //   console.log("DATOS" + datos);
    return datos;
}

export const Marca = async (key) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL2 + "/nmarcas", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}
export const Marcas = async (key) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL2 + "/marcas", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}
export const MarcaEXternal = async (key, nombre) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL2 + "/marcas/obtener/" + nombre, {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}

export const AutosCant = async (key) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL2 + "/autos", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}

export const FacCant = async (key) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL2 + "/facturaCani", {
        method: "GET",
        headers: cabeceras
    })).json();
    console.log(datos);
    return datos;
}



export const AutosNV = async (key) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL2 + "/autos/obtener/0", {
        method: "GET",
        headers: cabeceras
    })).json();
    console.log(datos);
    return datos;
}

export const AutosV = async (key) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL2 + "/autos/obtener/1", {
        method: "GET",
        headers: cabeceras
    })).json();
    console.log(datos);
    return datos;
}
export const ExternalPerson = async (key, iden) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL2 + "personas/obtener/iden/"+iden, {
        method: "GET",
        headers: cabeceras
    })).json();
    console.log(datos);
    return datos;
}


/*export const GuardarAuto = async (data, key) => {
    const headers = {
        "x-api-token": key        
    };
    const datos = await (await fetch(URL + "/autos/guardar", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}*/
export const GuardarAuto = async (data, key) => {
    const headers = {
        "x-api-token": key,
        "Content-Type": "application/json", // Asegúrate de incluir esta cabecera si no está presente en tu servidor
    };
    const requestOptions = {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    };

    try {
        const response = await fetch("http://localhost:3008/api/autos/guardar", requestOptions);
        const datos = await response.json();
        return datos;
    } catch (error) {
        console.log("Error:", error);
        // Manejo de errores
        throw error;
    }
};

export const ObtenerAuto= async (key, nroFila, uno) => {
    var cabeceras = { "x-api-token": key };
    var d = 'numeroF';
    if (uno === '1') {
        d = 'placa';
        console.log('a');
    }
    const datos = await (await fetch(URL2 + "/autos/obtener/" + d + "/" + nroFila, {
        method: "GET",
        headers: cabeceras
    })).json();
 //   console.log(datos);
    return datos;
}


export const ObtenerAutoDue= async (key, identificacion) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL2 + "/autos/obtener/persona/" + identificacion, {
        method: "GET",
        headers: cabeceras
    })).json();
//console.log(datos);
    return datos;
}
export const ListarRepuestos= async (key) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL2 + "/repuestos" , {
        method: "GET",
        headers: cabeceras
    })).json();
//console.log(datos);
    return datos;
}
export const DismiRepuestos = async (data, key) => {
    const headers = {
        "x-api-token": key,
        "Content-Type": "application/json", // Asegúrate de incluir esta cabecera si no está presente en tu servidor
    };
    const requestOptions = {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    };
    try {
        const response = await fetch(URL2+ "/repuestos/dismi", requestOptions);
        const datos = await response.json();
        return datos;
    } catch (error) {
        console.log("Error:", error);
        // Manejo de errores
        throw error;
    }
};
export const facturarRep = async (data, key) => {
    const headers = {
        "x-api-token": key,
        "Content-Type": "application/json", // Asegúrate de incluir esta cabecera si no está presente en tu servidor
    };
    const requestOptions = {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    };
    try {
        const response = await fetch(URL2+ "/facturar", requestOptions);
        const datos = await response.json();
        return datos;
    } catch (error) {
        console.log("Error:", error);
        // Manejo de errores
        throw error;
    }
};
/*
export const ObtenerAutoP = async (key, nroFila) => {
    var cabeceras = { "x-api-token": key };
    const datos = await (await fetch(URL2 + "/autos/obtener/placa/" + nroFila, {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}*/

export const ModificarAuto = async (key, data) => {
    const headers = {
        "x-api-token": key,
        "Content-Type": "application/json", // Asegúrate de incluir esta cabecera si no está presente en tu servidor
    };
    const requestOptions = {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    };

    try {
        const response = await fetch(URL2 + "autos/modificar", requestOptions);
        const datos = await response.json();
        return datos;
    } catch (error) {
        console.log("Error:", error);
        // Manejo de errores
        throw error;
    }
}