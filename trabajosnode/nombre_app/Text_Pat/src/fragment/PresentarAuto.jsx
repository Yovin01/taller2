import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Input } from 'react-bootstrap';
import RegistrarAuto from "./RegistrarAuto";
import DataTable from "react-data-table-component";
import React, { useState } from 'react';
import { Autos } from "../hooks/Conexion";
import { borrarSesion } from "../utilidades/Sessionutil";
import mensajes from "../utilidades/Mensajes";
import { useNavigate } from "react-router";
const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;


//var data

const columns = [
    {
        name: 'Modelo',
        selector: row => row.modelo,
    },
    {
        name: 'Año',
        selector: row => row.anio,
    },
    {
        name: 'Cilindraje',
        selector: row => row.cilindraje,
    },
    {
        name: 'Color',
        selector: row => row.color,
    },
    {
        name: 'Placa',
        selector: row => row.placa,
    },
    {
        name: 'Precio',
        selector: row => '$ '+row.precio,
    },
    {
        name: 'Marca',
        selector: row => row.marca,
    },
    {
        name: 'Acciones',
        selector: row => (<>
            <a href="/sesion" className="edit" title="Edit" data-toggle="tooltip"><i className="material-icons">Editar</i></a>
            <a href="#" className="delete" title="Delete" data-toggle="tooltip" style={{ color: "red" }}><i className="material-icons">Eliminar</i></a>

        </>),
    },
    
];


var data = [
    
]


export const Prueba = () => {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [data, setData] = useState([]);
    const navegation = useNavigate();
    
    Autos().then((info) =>{
        if(info.error==true && info.messaje == 'Acceso denegado. Token a expirado'){
            borrarSesion();
            mensajes(info.mensajes);
            navegation("/sesion")
        }else{
            setData(info.data);
        }
    })

    return (
        
        <div className="container ">
            <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                <div className="row ">

                    <div className="col-sm-3 mt-5 mb-4 text-gred">
                        <div className="search">
                            <form className="form-inline">
                                <input className="form-control mr-sm-2" type="search" placeholder="Buscar auto" aria-label="Search" />

                            </form>
                        </div>
                    </div>
                    <div className="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred" style={{ color: "blue" }}><h2><b>Autos registrados</b></h2></div>
                    <div className="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                        <Button variant="primary" onClick={handleShow}>
                            Agregar auto
                        </Button>
                    </div>
                </div>
                <div className="row">

                    <DataTable
                        columns={columns}
                        data={data}
                        selectableRows
                        
                    />

                </div>

                {/* <!--- Model Box ---> */}
                <div className="model_box">
                    <Modal
                        show={show}
                        onHide={handleClose}
                        backdrop="static"
                        keyboard={false}
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Agregar auto</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <RegistrarAuto />
                        </Modal.Body>

                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Cerrar
                            </Button>

                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
        </div>
    );
}


export default Prueba;