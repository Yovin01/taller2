import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import { useNavigate } from 'react-router';
import { Marca, ObtenerColores } from '../hooks/Conexion';
import { getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';

function RegistrarAuto() {
  const [validated, setValidated] = useState(false);
  const navegation = useNavigate();
  const [colores, setColores] = useState([]);
  const [marcas, setMarcas] = useState([]);
  const [llmarcas, setLlmarcas] = useState(false);
  const [llcolor, setLlcolor] = useState(false);

//acciones
// onsubmit
const onSubmit = (data) => {
  var datos = {
    "modelo": data.modelo,
    "anio": data.anio,
    "cilindraje": data.cilindraje,
  "marca" : data.marca,
  "precio": data.precio,
  "color": data.color,
  "placa": data.placa
  };
};
//color
  if(!llcolor){
    ObtenerColores().then((info)=>{
      if(info.error === false){
        console.log(info.data);
        setColores(info.data);
      }else{
        setColores(info.data);
        setLlcolor(true);
      }
    })
  }
  
  if(!llmarcas){
    Marca(getToken()).then((info)=>{
      if(info.error === false){
        console.log(info.data);
        setMarcas(info.data);
      }else{
        setMarcas(info.data);
        setLlmarcas(true);
      }
    
    })
  }

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
  };

  return (
    <Form noValidate validated={validated} onSubmit={handleSubmit}>
      <Row className="mb-3">
      <Form.Group as={Col} md="4" controlId="validationCustom03">
          <Form.Label>Año</Form.Label>
          <Form.Control type="text" placeholder="Ingrese Año" required />
          <Form.Control.Feedback type="invalid">
            Por favor, proporcione una año.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="5" controlId="validationCustom02">
        <Form.Label>Cilindraje</Form.Label>
          <Form.Control type="text" placeholder="Ingrese Cilindraje" required />
          <Form.Control.Feedback type="invalid">
            Por favor, proporcione cilindraje.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="4" controlId="validationCustom00">
        <Form.Label>Color</Form.Label>
          <select className='form-control form-control-user'>
            {colores.map((aux) => {
              return( <option key={aux.id} value={aux.id}>
                {aux.color}
              </option>)
            })}
          </select>
        </Form.Group>
      
        <Form.Group as={Col} md="4" controlId="validationCustom03">
          <Form.Label>Placa</Form.Label>
          <Form.Control type="text" placeholder="Ingrese Placa" required />
          <Form.Control.Feedback type="invalid">
            Por favor, proporcione una placa.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="4" controlId="validationCustom05">
          <Form.Label>Precio</Form.Label>
          <Form.Control type="text" placeholder="Ingrese Precio" required />
          <Form.Control.Feedback type="invalid">
          Por favor, proporcione un precio.
          </Form.Control.Feedback>
        </Form.Group>
      </Row>
      <Form.Group as={Col} md="4" controlId="validationCustom06">
      <Form.Label>Marcas</Form.Label>
      <Form.Control type="text" placeholder="Ingrese marca" required />
          <Form.Control.Feedback type="invalid">
          Por favor, proporcione una marca.
          </Form.Control.Feedback>
          <select className='form-control form-control-user'>
            {marcas.map((aux) => {
              return( <option key={aux.id} value={aux.id}>
                {aux.marca}
              </option>)
            })}
          </select>
        </Form.Group>
      <Button className="btn btn-success mt-4" type="submit" >Registrar auto</Button>
    </Form>
  );
}
export default RegistrarAuto;