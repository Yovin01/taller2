const URL = "http://localhost/v1/index.php"
export const InicioSesion = async (data) => {

    const datos = await (await fetch(URL + "/sesion", {
        method: "POST",
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}

export const Marca = async (key) => {
    var cabeceras = { "X-API-KEY": key };
    const datos = await (await fetch(URL + "/marca", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}

export const GUardarAuto = async (data, key) => {
    const headers = { "X-API-KEY": key };
    const datos = await (await fetch(URL + "/auto/guardar", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
  }

export const AutosCant = async(key) => {
    var cabeceras = { "X-API-KEY": key };
    const datos = await (await fetch(URL + "/auto/contar", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}
 

export const Autos = async () => {
    const datos = await (await fetch(URL + "/auto", {
        method: "GET"
    })).json();
    console.log(datos);
    return datos;
}

export const ObtenerColores = async()=>{
    const datos = await (await fetch(URL + "/auto/colores", {
        method: "GET",
    })).json();
    return datos;
}
