var express = require('express');
var router = express.Router();
const { body, validationResult } = require('express-validator');
const RolController = require('../controls/RolController');
var rolController = new RolController();
const PersonaController = require('../controls/PersonaController');
var personaController = new PersonaController();
const AutoController = require('../controls/AutoController');
var autoController = new AutoController();
const CuentaController = require('../controls/CuentaController');
var cuentaController = new CuentaController();
const MarcaController = require('../controls/MarcaController');
var marcaController = new MarcaController();
let jwt = require('jsonwebtoken');
var models = require('../models/');
const RepuestoController = require('../controls/RepuestoController');
var repuestoController = new RepuestoController();
/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});
// middware
var auth = function middware(req, res, next) {
  const token = req.headers['x-api-token'];
  if (token) {
    require('dotenv').config();
    const llave = process.env.KEY;
    jwt.verify(token, llave, async (err, decoded) => {
      if (err) {
        res.status(401);
        res.json({ msg: "Token no valido", code: 401 });
      } else {
        var cuenta = models.cuenta;
        req.decoded = decoded;
        let aux = await cuenta.findOne({ where: { external_id: req.decoded.external }});
        if (aux === null) {

          res.status(401);
          res.json({ msg: "Token no valido", code: 401 });

        } else {
          next();
        }
      }

    });
  } else {
    res.status(401);
    res.json({ msg: "No existe token", code: 401 });
  }

}



//GET
router.get('/roles', rolController.listar);
router.get('/personas',auth, personaController.listar);
router.get('/personas/obtener/:external', personaController.obtener);
router.get('/personas/obtener/iden/:iden', auth,personaController.obtenerExt);
router.get('/marcas/obtener/:monbre', marcaController.obtener);
router.get('/autos/obtener/:vendido', autoController.listar);
router.get('/autos/obtener/numeroF/:numeroF', autoController.obtener);
router.get('/autos/obtener/placa/:placa', autoController.obtener);
router.get('/autos/obtener/persona/:identificacion', autoController.obtenerDue);
router.get('/autos', auth,autoController.listarN);
router.get('/repuestos', auth,repuestoController.listar);
//POST
router.post('/repuestos/dismi',auth, repuestoController.cantidadDim);
router.post('/facturar',auth,  repuestoController.facturar);
router.get('/facturaCani', auth, repuestoController.listarF);
router.post('/personas/guardar', [
  body('apellidos', 'Ingrese sus apellidos').trim().exists().not().isEmpty().isLength({ min: 3, max: 50 }).withMessage("Ingrese un valor mayor o igual a 3 y menor a 50"),
  body('nombres', 'Ingrese sus nombres').trim().exists().not().isEmpty().isLength({ min: 3, max: 50 }).withMessage("Ingrese un valor mayor o igual a 3 y menor a 50"),
], personaController.guardar);
router.post('/personas/modificar', personaController.modificar);
router.post('/autos/guardar', [
  body('placa', 'Ingrese la placa').trim().exists().not().isEmpty().isLength({ min: 6, max: 7 }).withMessage("Ingrese una placa desde 6 a 7 caracteres")
], autoController.guardar);
router.post('/autos/modificar', [
  body('placa', 'Ingrese la placa').trim().exists().not().isEmpty().isLength({ min: 6, max: 7 }).withMessage("Ingrese una placa desde 6 a 7 caracteres")
],auth ,autoController.modificar);
router.post('/sesion', [
  body('correo', 'Ingrese un correo valido').trim().exists().not().isEmpty().isEmail().withMessage("Ingrese un correo")
  , body('clave', 'Ingrese una clave valida').trim().exists().not().isEmpty().withMessage("Ingrese una calve")
], cuentaController.sesion);

//MARCA
router.get('/marcas',auth,marcaController.listar);
router.get('/nmarcas',auth,marcaController.listarN);
router.post('/marca/guardar',auth,marcaController.guardar);
router.post('/marca/modificar/:external',auth,marcaController.modificar);
router.get('/marcas/num',auth,marcaController.numMarca);

/*router.get('/sumar/:a/:b', function (req, res, next) {
  var a = Number(req.params.a);
  var b = Number(req.params.b);
  var c = a + b;
  res.status(200);
  res.json({ "msg": "OK", "resp": c });
});

router.post('/sumar', function (req, res, next) {
  var a = Number(req.body.a);
  var b = Number(req.body.b);
  if (isNaN(a) || isNaN(b)) {
    res.status(400);
    res.json({ "msg": "FALTAN DATOS"});
  }
  var c = a + b;
  res.status(200);
  res.json({ "msg": "OK", "resp": c });

});*/

module.exports = router;
