'use strict';
var models = require('../models/');
var persona = models.persona;
var auto = models.auto;
var marca = models.marca;
var detalle = models.detalle;
var factura = models.factura;
const { body, validationResult, check } = require('express-validator');

class AutoController {

    async listarN(req, res) {
        var listar = await auto.count();
        res.json({ msg: 'OK!', code: 200, info: listar });
    }
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var person_id = null;
            if (req.body.identificacion != undefined) {
                person_id = req.body.identificacion;
            }
            var marca_external = req.body.externalM;
            //   console.log("aaaaaaaaa");
            if (marca_external != undefined) {
                let personAux = await persona.findOne({ where: { identificacion: person_id } });
                let marcaAux = await marca.findOne({ where: { external_id: marca_external } });

                let transaction = await models.sequelize.transaction();
                if (personAux && req.body.numeroF != undefined) {

                    var data = {
                        numeroF: req.body.numeroF,
                        precioTotal: req.body.precioTotal,
                        detalle: req.body.detalle,
                        id_persona: personAux.id,
                        auto: {
                            placa: req.body.placa,
                            color: req.body.color,
                            modelo: req.body.modelo,
                            precio: req.body.precio,
                            id_marca: marcaAux.id,
                            id_persona: personAux.id,
                        }
                    }
                    try {
                        await factura.create(data, {
                            include: [{
                                model: models.auto,
                                as: "auto"
                            }], transaction
                        });
                        await transaction.commit();
                        res.json({
                            msg: "SE HAN REGISTRADO SUS DATOS",
                            code: 200
                        });

                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else if (marcaAux) {

                    var datas = {
                        placa: req.body.placa,
                        color: req.body.color,
                        modelo: req.body.modelo,
                        precio: req.body.precio,
                        id_marca: marcaAux.id,
                        estado: 0
                    }
                    try {
                        await auto.create(datas, {
                            transaction
                        });
                        await transaction.commit();
                        res.json({
                            msg: "SE HAN REGISTRADO SUS DATOS",
                            code: 200
                        });

                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
                console.log(req.body);
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }

    }

    async modificar(req, res) {
        var carro = await auto.findOne({ where: { placa: req.body.placa } });
        let personAux = await persona.findOne({ where: { id: carro.id_persona } });;
        let marcaAux = await marca.findOne({ where: { id: carro.id_marca } });

        if (req.body.externalPersona != undefined) {
            personAux = await persona.findOne({ where: { external_id: req.body.externalPersona } });
        }
        if (req.body.externalM != undefined) {
            marcaAux = await marca.findOne({ where: { external_id: req.body.externalM } });
        }

        if (carro === null) {
            res.status(400);
            res.json({
                msg: "NO EXISTEN REGISTROS",
                code: 400
            });
        } else {
            var uuid = require('uuid');
            carro.placa = req.body.placa,
                carro.color = req.body.color,
                carro.modelo = req.body.modelo,
                carro.precio = req.body.precio,
                carro.id_marca = marcaAux.id,
                carro.id_persona = personAux.id,
                carro.external_id = uuid.v4();
            var result = await carro.save();
            if (result === null) {
                res.status(400);
                res.json({
                    msg: "NO SE HAN MODIFICADO SUS DATOS",
                    code: 400
                });
            } else {
                res.status(200);
                res.json({
                    msg: "SE HAN MODIFICADO SUS DATOS CORRECTAMENTE",
                    code: 200
                });
            }
        }
    }

    async listar(req, res) {
        const vendido = req.params.vendido;
        if (vendido == 0) {
            try {
                const listar = await auto.findAll({
                    where: { estado: false },
                    attributes: ['color', 'placa', 'modelo', 'precio'],
                    include: [{
                        model: models.marca,
                        attributes: ['nombre']
                    }]
                });
                res.status(200);
                res.json({ msg: 'OK!', code: 200, info: listar });
            } catch (error) {
                res.status(500).json({ error: 'Error al obtener los datos.' });
            }

        } else {
            try {
                const listar = await factura.findAll({
                    attributes: ['numeroF', 'precioTotal', 'detalle'],
                    raw: true,
                    include: [
                        {
                            model: models.auto,
                            as: 'auto',
                            attributes: ['color', 'placa', 'modelo', 'precio'],
                            include: [{
                                model: marca,
                                attributes: ['nombre']
                            }]
                        }
                        , {
                            model: models.persona,
                            attributes: ['nombres', 'apellidos', 'identificacion']
                        }
                    ]
                });
                res.status(200);
                res.json({ msg: 'OK!', code: 200, info: listar });
            } catch (error) {
                res.status(500).json({ error: 'Error al obtener los datos.' });
            }
        }
    }
    async obtener(req, res) {
        try {
            console.log(req.params.numeroF);
            if (req.params.numeroF !== undefined) {
                const listar = await factura.findOne({
                    where: { numeroF: req.params.numeroF },
                    attributes: ['numeroF', 'precioTotal', 'detalle'],
                    include: [
                        {
                            model: models.auto,
                            as: 'auto',
                            attributes: ['color', 'placa', 'modelo', 'precio'],
                            include: [{
                                model: models.marca,
                                attributes: ['nombre']
                            }]
                        }
                        , {
                            model: models.persona,
                            as: 'persona',
                            attributes: ['nombres', 'apellidos', 'identificacion']
                        }
                    ]
                });
                if (listar === null) {
                    listar = {};
                }
                res.status(200);
                res.json({ msg: 'OK!', code: 200, info: listar });
            } else {
                const listar = await auto.findOne({
                    where: { placa: req.params.placa },
                    attributes: ['color', 'placa', 'modelo', 'precio'],
                    include: [{
                        model: models.marca,
                        attributes: ['nombre']
                    }]
                });
                res.status(200);
                res.json({ msg: 'OK!', code: 200, info: listar });

            }

        } catch (error) {
            res.status(500).json({ error: 'Error al obtener los datos.' });
        }

    }

    async obtenerDue(req, res) {
        try {
            var listar = {};
            //   console.log(req.params.identificacion);
            if (req.params.identificacion !== undefined) {
                listar = await persona.findAll({
                    where: { identificacion: req.params.identificacion },
                    attributes: ['apellidos', 'identificacion', 'external_id'],
                    include: [
                        {
                            model: models.auto,
                            as: 'auto',
                            attributes: ['color', 'placa', 'modelo', 'precio'],
                            include: [{
                                model: models.marca,
                                attributes: ['nombre']
                            }, {
                                model: models.factura,
                                as: 'factura',
                                attributes: ['numeroF', 'precioTotal', 'detalle']
                            }]
                        }

                    ]
                });
                console.log(listar);
                res.status(200);
                res.json({ msg: 'OK!', code: 200, info: listar });
            } else {
                if (listar === null) {
                    listar = {};
                }
                res.status(200);
                res.json({ msg: 'OK!', code: 200, info: listar });

            }

        } catch (error) {
            res.status(500).json({ error: 'Error al obtener los datos.' });
        }

    }

}
module.exports = AutoController;