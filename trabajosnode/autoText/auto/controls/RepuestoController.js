'use strict';

const { validationResult } = require('express-validator');
var models = require('../models/');
const factura = require('../models/factura');
class RepuestoController {
    async listar(req, res) {
        var listar = await models.repuesto.findAll({
            attributes: ['nombre', 'external_id', 'marca', 'precio', 'tipo_categoria', 'cantidad']
        });
        res.json({ msg: 'OK!', code: 200, info: listar });
    }
    async listarF(req, res) {
        var listar = await models.factura.count();
        res.json({ msg: 'OK!', code: 200, info: listar });
    }
    async obtenerRepuesto(req, res) {
        var listar = await models.repuesto.findOne({
            where: { external_id: req.external_id },
            attributes: ['nombre', 'external_id', 'marca', 'precio', 'tipo_categoria']
        });
        res.json({ msg: 'OK!', code: 200, info: listar });
    }

    async cantidadDim(req, res) {
        var listar = await models.repuesto.findOne({ where: { external_id: req.body.external_id } });
        if (listar === null) {
            res.status(400);
            res.json({
                msg: "NO EXISTEN REGISTROS",
                code: 400
            });
        } else {
            if (listar.cantidad >= req.body.cantidad) {
                listar.cantidad = listar.cantidad - req.body.cantidad;
                var result = await listar.save();
                if (result === null) {
                    res.status(400);
                    res.json({
                        msg: "NO SE HAN MODIFICADO SUS DATOS",
                        code: 400
                    });
                } else {
                    res.status(200);
                    res.json({
                        msg: "SE HAN MODIFICADO SUS DATOS CORRECTAMENTE",
                        code: 200
                    });
                }
            } else {
                res.status(400);
                res.json({
                    msg: "No hay suficiientes repuestos",
                    code: 400
                });
            }
        }
    }
    async  facturar(req, res) {
        try {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(400).json({ msg: "Datos faltantes", code: 400, errors: errors });
          }
                const personId = req.body.identificacion;
          let personAux = await models.persona.findOne({ where: { identificacion: personId } });
      
          if (!personAux || !req.body.numeroF) {
            return res.status(400).json({ msg: "Datos de persona o número de factura no válidos", code: 400 });
          }
               const data = {
            numeroF: req.body.numeroF,
            precioTotal: req.body.precioTotal,
            detalle: req.body.detalle,
            id_persona: personAux.id,
          };
                await models.factura.create(data);
                return res.json({ msg: "Se han registrado los datos de factura", code: 200 });
        } catch (error) {
          if (error.errors && error.errors[0].message) {
            return res.json({ msg: error.errors[0].message, code: 400 });
          } else {
            return res.json({ msg: error.message, code: 400 });
          }
        }
      }
      

}

module.exports = RepuestoController;