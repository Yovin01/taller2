'use strict';
const { UUIDV4 } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    const auto = sequelize.define('auto', {
        placa: { type: DataTypes.STRING(8), defaultValue: "NO_DATA",unique: true },
        color: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
     //   marca: {type: DataTypes.STRING(50), defaultValue: "NO_DATA"},
     precio: { type: DataTypes.DECIMAL,allowNull: false  }, 
        modelo: {type: DataTypes.STRING(50), defaultValue: "NO_DATA"},
        estado:{type: DataTypes.BOOLEAN, defaultValue: true}
    }, {
        freezeTableName: true
    });

    auto.associate = function (models){
    auto.belongsTo(models.persona, {foreignKey: 'id_persona'});
    auto.belongsTo(models.marca, {foreignKey: 'id_marca'});
    auto.belongsTo(models.factura, {foreignKey: 'id_factura'});
    }

    return auto;
};