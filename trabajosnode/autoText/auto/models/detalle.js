'use strict';
const { UUIDV4 } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    const detalle = sequelize.define('detalle', {
        producto: { type: DataTypes.STRING(40)},
        precio: { type: DataTypes.DECIMAL,allowNull: false  },
         cantidad:{type: DataTypes.INTEGER, defaultValue: 1}
    },{
        timestamps: false,  freezeTableName: true
    });
    detalle.associate = function (models){
  //  detalle.belongsTo(models.factura, {foreignKey: 'id_factura'});
  //  detalle.belongsTo(models.auto, {foreignKey: 'id_auto'});
    }

    return detalle;
};