'use strict';
const { UUIDV4 } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    const factura = sequelize.define('factura', {
        numeroF: { type: DataTypes.STRING(15), unique: true },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        estado:{type: DataTypes.BOOLEAN, defaultValue: true},
        precioTotal: { type: DataTypes.DECIMAL,allowNull: false  }, 
        detalle: { type: DataTypes.JSON}
    }, {
        freezeTableName: true
    });

    factura.associate = function (models){
    factura.belongsTo(models.persona, {foreignKey: 'id_persona'});
    factura.hasMany(models.auto, {foreignKey: 'id_factura', as: 'auto'});
    }

    return factura;
};